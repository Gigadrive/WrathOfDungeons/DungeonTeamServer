package net.wrathofdungeons.dungeonteamserver.cmd;

import net.wrathofdungeons.dungeonapi.DungeonAPI;
import net.wrathofdungeons.dungeonapi.cmd.manager.Command;
import net.wrathofdungeons.dungeonapi.user.Rank;
import net.wrathofdungeons.dungeonapi.user.User;
import net.wrathofdungeons.dungeonteamserver.DungeonTeamServer;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;

public class SyncCommand extends Command {
    private boolean MAY_SAVE_WORLD = true;

    public SyncCommand(){
        super("sync", Rank.ADMIN);
    }

    private void saveWorld(Player p, World currentWorld, String folder) throws Exception {
        File file = new File(folder);
        if(file.exists() || file.isDirectory()){
            p.sendMessage(ChatColor.GREEN + "Found old file - deleting..");
            FileUtils.deleteDirectory(file);
        } else {
            p.sendMessage(ChatColor.GREEN + "Old file not found - skipping delete step.");
        }

        p.sendMessage(ChatColor.GREEN + "Copying world to target directory..");

        String d = Bukkit.getWorldContainer().getAbsolutePath().endsWith("/") ? null : "/";
        FileUtils.copyDirectory(new File(Bukkit.getWorldContainer().getAbsolutePath() + d + currentWorld.getName() + "/"),file);

        File worldDataFile = new File(file.getAbsolutePath() + "uid.dat");
        if (worldDataFile.exists()) {
            worldDataFile.delete();
            FileUtils.forceDelete(worldDataFile);
        }

        p.sendMessage(ChatColor.GREEN + "Done!");
    }

    @Override
    public void execute(Player p, String label, String[] args) {
        User u = User.getUser(p);

        if(MAY_SAVE_WORLD){
            World currentWorld = p.getWorld();

            if(currentWorld.getName().equalsIgnoreCase("wodlobby")){
                MAY_SAVE_WORLD = false;
                p.sendMessage(ChatColor.GREEN + "Forcing world save..");

                Bukkit.dispatchCommand(Bukkit.getConsoleSender(),"save");

                new BukkitRunnable(){
                    @Override
                    public void run() {
                        DungeonAPI.async(() -> {
                            try {
                                p.sendMessage(ChatColor.GREEN + "Synchronizing world..");

                                saveWorld(p,currentWorld,"/home/wod/wrapper/local/templates/Lobby/default/wodlobby/");
                                MAY_SAVE_WORLD = true;
                            } catch(Exception e){
                                p.sendMessage(ChatColor.RED + "An error occurred.");
                                e.printStackTrace();
                                MAY_SAVE_WORLD = true;
                            }
                        });
                    }
                }.runTaskLater(DungeonTeamServer.getInstance(),2*20);
            } else {
                MAY_SAVE_WORLD = false;
                p.sendMessage(ChatColor.GREEN + "Forcing world save..");

                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save");

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        DungeonAPI.async(() -> {
                            try {
                                p.sendMessage(ChatColor.GREEN + "Synchronizing world..");

                                saveWorld(p, currentWorld, "/home/wod/wrapper/local/templates/Test/default/" + currentWorld.getName() + "/");
                                MAY_SAVE_WORLD = true;
                            } catch (Exception e) {
                                p.sendMessage(ChatColor.RED + "An error occurred.");
                                e.printStackTrace();
                                MAY_SAVE_WORLD = true;
                            }
                        });
                    }
                }.runTaskLater(DungeonTeamServer.getInstance(), 2 * 20);
            }
        } else {
            p.sendMessage(ChatColor.RED + "There is already a sync running right now.");
        }
    }
}
