package net.wrathofdungeons.dungeonteamserver.generator;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.ChunkGenerator;

import java.util.Random;

// https://bukkit.org/threads/79066
public class DungeonWorldGenerator extends ChunkGenerator {
    @Override
    public ChunkData generateChunkData(World world, Random random, int chunkX, int chunkZ, BiomeGrid biome) {
        ChunkData chunk = createChunkData(world);

        for (int X = 0; X < 16; X++)
            for (int Z = 0; Z < 16; Z++) {
                for (int i = 0; i < 25; i++) {
                    if (t(i) != Material.AIR)
                        chunk.setBlock(X, i, Z, t(i));
                }
            }
        return chunk;
    }

    private Material t(int y) {
        switch (y) {
            case 24:
                return Material.WATER;
            case 23:
                return Material.WATER;
            case 22:
                return Material.WATER;
            case 21:
                return Material.WATER;
            case 20:
                return Material.WATER;
            case 19:
                return Material.WATER;
            case 18:
                return Material.WATER;
            case 17:
                return Material.SAND;
            case 16:
                return Material.SAND;
            case 15:
                return Material.SAND;
            case 14:
                return Material.SAND;
            case 13:
                return Material.SAND;
            case 12:
                return Material.STONE;
            case 11:
                return Material.STONE;
            case 10:
                return Material.STONE;
            case 9:
                return Material.STONE;
            case 8:
                return Material.STONE;
            case 7:
                return Material.STONE;
            case 6:
                return Material.STONE;
            case 5:
                return Material.STONE;
            case 4:
                return Material.STONE;
            case 3:
                return Material.STONE;
            case 2:
                return Material.STONE;
            case 1:
                return Material.STONE;
            case 0:
                return Material.BEDROCK;
            default:
                return Material.AIR;
        }
    }
}
