package net.wrathofdungeons.dungeonteamserver.listener;

import net.wrathofdungeons.dungeonteamserver.DungeonTeamServer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class WeatherChangeListener implements Listener {
    public static ArrayList<String> LOADED = new ArrayList<String>();

    @EventHandler
    public void onInit(WorldInitEvent e) {
        if (!LOADED.contains(e.getWorld().getName())) {
            e.getWorld().setStorm(false);
            e.getWorld().setThundering(false);
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                if (!LOADED.contains(e.getWorld().getName()))
                    LOADED.add(e.getWorld().getName());
            }
        }.runTaskLater(DungeonTeamServer.getInstance(), 20);
    }

    @EventHandler
    public void onChange(WeatherChangeEvent e) {
        if (LOADED.contains(e.getWorld().getName()))
            e.setCancelled(true);
    }
}
