package net.wrathofdungeons.dungeonteamserver;

import net.wrathofdungeons.dungeonapi.DungeonAPI;
import net.wrathofdungeons.dungeonteamserver.cmd.SyncCommand;
import net.wrathofdungeons.dungeonteamserver.generator.DungeonWorldGenerator;
import net.wrathofdungeons.dungeonteamserver.listener.PlayerJoinListener;
import net.wrathofdungeons.dungeonteamserver.listener.WeatherChangeListener;
import org.bukkit.Bukkit;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

public class DungeonTeamServer extends JavaPlugin {
    private static DungeonTeamServer instance;

    public void onEnable(){
        if(!DungeonAPI.getServerName().equals("TeamServer-1")){
            Bukkit.shutdown();
            return;
        }

        instance = this;
        new SyncCommand();

        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new WeatherChangeListener(), this);
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return worldName.equalsIgnoreCase("wod") ? new DungeonWorldGenerator() : super.getDefaultWorldGenerator(worldName, id);
    }

    public static DungeonTeamServer getInstance() {
        return instance;
    }
}
